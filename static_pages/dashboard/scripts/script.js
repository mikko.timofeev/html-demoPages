$(document). ready
(
	function ()
	{
		setFooter2 ();
		tableInit ('candidates');
		tableSwitch ('candidates');
		tableInit ('employers');
	}
);

//window.alert = function () { }; // so that no alerts are displayed
//window. onload = function (){}

window. onhashchange = setFooter2;

function setFooter2 (e) // version with jQuery
{
	$ ('.displayed'). removeClass ('displayed');
	$ ('.chosenLink'). removeClass ('chosenLink');
	var newChoiceId = window. location. hash,
		skipScroll = false;
	
	if (!($ (newChoiceId). hasClass ('chosableLink')))
	{
		newChoiceId = '#' + $ ('#defaultChoice') [0]. value;
		skipScroll = true;
	}
	
	$ (newChoiceId). addClass ('displayed');
	$ ('#link_' + newChoiceId. substr (1)). addClass ('chosenLink');
	
	if (skipScroll === true)
	{
		history. pushState ('', document. title, window. location. pathname);
		//e. preventDefault ();
	}
	else //if (!skipScroll)
	{
		//$ (newChoiceId). get (0). scrollIntoView ();
		$('html, body'). animate
		(
			{
				scrollTop: $ (newChoiceId). offset (). top
			},
			1000
		);
		window. location. hash = newChoiceId;
	}
}

function selectAll (el)
{
	var id = getActiveTableId ();
	var checkboxes = $('#' + id + ' tbody tr td input[type="checkbox"]');

	for (var i = 0; i < checkboxes. length; i += 1)
	{
		if (checkboxes [i]. type == 'checkbox')
		{
			checkboxes [i]. checked = el. checked;
			activateRow (checkboxes [i]);
		}
	}
}

function tableInit(id) {
	$('#' + id).DataTable
	(
		{
			"order": [[5, "desc"]],
			"columns":
			[
				{ "orderDataType": "dom-checkbox" },
				null,
				null,
				null,
				null,
				null,
				null,
				null
			],
			"dom": 't',
			"scrollX": true,
			"scrollCollapse": true
		}
	);
}

function tableSwitch (id) // switching to table by id
{
	var aRole = $("#" + id + "_filter") [0]; // input[type='radio']

	$('.oneTable'). removeClass ('oneTable');
	$('#' + id + '_wrapper'). addClass ('oneTable');

	var totalP = $('#' + id). DataTable (). page. info (). pages; // total number of pages
	$('#page_total') [0]. innerHTML = totalP;
	$('#page_this'). attr ("max", totalP);
	if (totalP === 1)
	{
		$('#paginator'). addClass ('hidden');
	}
	else
	{
		$('#paginator'). removeClass ('hidden');
	}

	var totalR = $('#' + id). DataTable (). page. info (). recordsTotal; // results per page
	$('#page_rows'). attr ("max", totalR);
	$('#users_total') [0]. innerHTML = totalR + ' ' + getActiveTableId ();

	$('#searchField'). attr ("placeholder", 'Search ' + getActiveTableId ());

	$('#' + id). DataTable (). columns. adjust (). draw (); // redraw the table!
}

function getActiveTableId ()
{
	return $(".filters input[type='radio']:checked") [0]. id. split ('_')[0];
}

function page (val)
{
	var id = getActiveTableId ();
	if (val > 0)
	{
		val -= 1;
	}
	$('#' + id). DataTable (). page (val). draw ('page');

	var currentPage = $('#' + id). DataTable (). page (),
		totalPages = $('#' + id). DataTable (). page. info (). pages;

	$('#page_this') [0]. value = currentPage + 1; // update page number

	 // disable buttons if needed (ex. 'prev' button on first page)
	if (currentPage + 1 === totalPages)
	{
		$('#page_next'). attr ("disabled", "disabled");
	}
	else
	{
		$('#page_next'). removeAttr ("disabled");
	}

	if (currentPage === 0)
	{
		$('#page_prev'). attr ("disabled", "disabled");
	}
	else {
		$('#page_prev'). removeAttr ("disabled");
	}
}

function rows (val) // to set amount of rows per page
{
	var id = getActiveTableId ();
	$('#' + id). DataTable (). page. len (val). draw ();
	tableSwitch (id);
	page (1);
}

function activateRow (el) // when we tick checkbox
{
	if ($(el) [0]. checked) {
		$(el). parent (). parent (). addClass ('active');
	}
	else
	{
		$(el). parent (). parent (). removeClass ('active');
	}
}

function searchPeople (el) // searchbar functionality
{
	$('#' + getActiveTableId ()). DataTable (). search (el. value). draw ();
}

function mutate (el, newType) // to change input's type for beauty
{
	el. type = newType;
}