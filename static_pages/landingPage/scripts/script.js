window. onload = setFooter;
window. onhashchange = setFooter;
function setFooter (e)
{
	document. getElementsByClassName ('displayed') [0]. removeAttribute ('class');
	document. getElementsByClassName ('chosenLink') [0]. removeAttribute ('class');
	var showNow = window. location. hash. substr(1), skipScroll = false;
	switch (showNow)
	{
		case 'overview':
		case 'features':
		case 'techSpecs':
		case 'testimonials':
		case 'pricing':
			break;
		default:
			skipScroll = true;
			showNow = 'overview';
	}
	
	document. getElementById (showNow). setAttribute ('class', 'displayed');
	document. getElementById ('link_' + showNow). setAttribute ('class', 'chosenLink');
	
	if (skipScroll === true)
	{
		history. pushState ('', document. title, window. location. pathname);
		e. preventDefault ();
	}
	else //if (!skipScroll)
	{
		document. getElementById (showNow). scrollIntoView ();
		history. pushState ('', document. title, window. location. pathname);
	}
}
